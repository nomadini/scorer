#Scorer

Creates SGD models from ad history data and persists them in cassandra
, those models are used by Scorer service.

### Internal details explanation
    ScoringMessageProcessor.cpp         reads messages from kafka queue, runs a pipleline of logic for each request, which involves 
                                        fetching device browing histories, calculating score based on different models, creates segment and persist 
                                        those segments in cassandra.
                                        
    Modules package
                                        contains all the different modules that are run by ScoringMessageProcessor.cpp  
    