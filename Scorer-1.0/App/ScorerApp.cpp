//
// Created by Mahmoud Taabodi on 7/11/16.
//
#include "ScorerAppStarter.h"
#include <memory>
#include <string>
int main(int argc, char* argv[]) {
        std::shared_ptr<ScorerAppStarter> scorerApp = std::make_shared<ScorerAppStarter>();
        scorerApp->startTheApp(argc, argv);

}
