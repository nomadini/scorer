//
// Created by Mahmoud Taabodi on 7/11/16.
//


#include "GUtil.h"
#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "TempUtil.h"
#include "LogLevelManager.h"
#include <thread>
#include "GUtil.h"
#include "StringUtil.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "ScoreToSegmentConverter.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "MySqlAdvertiserModelMappingService.h"
#include "ConcurrentQueueFolly.h"
#include "MySqlModelService.h"
#include <boost/foreach.hpp>
#include "MySqlTargetGroupService.h"
#include "MySqlCampaignService.h"
#include "MySqlSegmentService.h"
#include "CollectionUtil.h"

#include <thread>
#include "CassandraDriverInterface.h"
#include "ScoringMessageProcessor.h";
#include "Poco/NotificationQueue.h"
#include "DeviceSegmentHistoryCassandraService.h"
#include "SegmentToDeviceCassandraService.h"
#include "KafkaConsumer.h"
#include "DeviceSegmentPersistenceModule.h"
#include "SegmentToDevicePersistenceModule.h"
#include "SegmentCreatorModule.h"
#include "CalculateScoreModule.h"

#include "DeviceFeatureFetcherModule.h"
#include "ScorerHealthService.h"
#include "ScorerAsyncJobsService.h"
#include "DataReloadService.h"
#include "AsyncThreadPoolService.h"

#include "ServiceFactory.h"
#include "BeanFactory.h"
#include "ScorerAppStarter.h"
#include "EntityToModuleStateStats.h"
#include "LastTimeSeenSource.h"
#include "ScoredInLastNMinutesModule.h"
#include "CrossWalkDeviceSegmentPersistenceModule.h"

void ScorerAppStarter::startTheApp(int argc, char* argv[]) {
        auto scoringMessageProcessor = std::make_shared<ScoringMessageProcessor> ();

        std::string appName = "Scorer";
        TempUtil::configureLogging (appName, argv);
        std::string propertyFileName = "scorer.properties";
        scoringMessageProcessor->beanFactory = std::make_unique<BeanFactory>("SNAPSHOT");
        auto beanFactory = scoringMessageProcessor->beanFactory.get();
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;
        beanFactory->initializeModules();
        auto serviceFactory = std::make_unique<ServiceFactory>(beanFactory);
        serviceFactory->initializeModules();


        LOG(INFO) << "google log starting the scorer : argc "<< argc << " argv : " <<  *argv;

        int segmentPersistenceInMySqlIntervalInSeconds = 10;
        beanFactory->configService->get("segmentPersistenceInMySqlIntervalInSeconds",
                                        segmentPersistenceInMySqlIntervalInSeconds);

        auto isMessageProcessorReadyToProcess = std::make_shared<gicapods::AtomicBoolean>(false);

        auto modelIdToModelScoreLevelMap =
                std::make_shared<gicapods::ConcurrentHashMap<int, ModelScoreLevelContainer > > ();;

        auto scorerHealthFlag = std::make_shared<gicapods::AtomicBoolean>(true);


        auto numberOfExceptionsInScorerMessageProcessor = std::make_shared<gicapods::AtomicLong>();

        auto calculateScoreModule = std::make_shared<CalculateScoreModule>();
        calculateScoreModule->entityToModuleStateStats =
                beanFactory->entityToModuleStateStats.get();

        auto scoreToSegmentConverter = ScoreToSegmentConverter::getInstance(beanFactory->configService.get());
        auto segmentCreatorModule = std::make_shared<SegmentCreatorModule>();
        segmentCreatorModule->entityToModuleStateStats =
                beanFactory->entityToModuleStateStats.get();

        segmentCreatorModule->scoreToSegmentConverter = scoreToSegmentConverter.get();

        segmentCreatorModule->modelScoreCassandraService =
                beanFactory->modelScoreCassandraService.get();
        segmentCreatorModule->modelIdToModelScoreLevelMap = modelIdToModelScoreLevelMap;

        auto deviceSegmentPersistenceModule =
                std::make_shared<DeviceSegmentPersistenceModule>
                        (beanFactory->deviceSegmentHistoryCassandraService.get());
        deviceSegmentPersistenceModule->entityToModuleStateStats =
                beanFactory->entityToModuleStateStats.get();




        auto crossWalkDeviceSegmentPersistenceModule =
                std::make_shared<CrossWalkDeviceSegmentPersistenceModule>
                        (beanFactory->deviceSegmentHistoryCassandraService.get());
        crossWalkDeviceSegmentPersistenceModule->entityToModuleStateStats =
                beanFactory->entityToModuleStateStats.get();

        crossWalkDeviceSegmentPersistenceModule->crossWalkReaderModule =
                beanFactory->crossWalkReaderModule.get();

        auto segmentToDevicePersistenceModule =
                std::make_shared<SegmentToDevicePersistenceModule>
                        (beanFactory->segmentToDeviceCassandraService.get());


        auto deviceFeatureFetcherModule = std::make_shared<DeviceFeatureFetcherModule>();
        deviceFeatureFetcherModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        deviceFeatureFetcherModule->deviceFeatureHistoryCassandraService = beanFactory->deviceFeatureHistoryCassandraService.get();
        segmentToDevicePersistenceModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();


        std::shared_ptr<tbb::concurrent_hash_map<int, std::shared_ptr<ModelRequest> > >
        allLoadedModels = std::make_shared<tbb::concurrent_hash_map<int, std::shared_ptr<ModelRequest> > > ();

        std::shared_ptr<gicapods::AtomicLong> consumerTimeout = std::make_shared<gicapods::AtomicLong>();
        std::shared_ptr<gicapods::AtomicLong> consumingMsgFailed = std::make_shared<gicapods::AtomicLong>();
        std::shared_ptr<gicapods::AtomicLong> consumerReadMessage = std::make_shared<gicapods::AtomicLong>();
        std::shared_ptr<gicapods::AtomicLong> endOfPartitionReached = std::make_shared<gicapods::AtomicLong>();
        std::shared_ptr<gicapods::AtomicLong> lastOffsetRead = std::make_shared<gicapods::AtomicLong>();

        std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInScorerMessageProcessorLimit = std::make_shared<gicapods::AtomicLong>();
        numberOfExceptionsInScorerMessageProcessorLimit->setValue(1000000); //TODO make this property driven

        auto scorerHealthService = std::make_shared<ScorerHealthService>();
        scorerHealthService->numberOfExceptionsInScorerMessageProcessorLimit =
                numberOfExceptionsInScorerMessageProcessorLimit;
        scorerHealthService->consumerTimeout = consumerTimeout;
        scorerHealthService->lastOffsetRead = lastOffsetRead;
        scorerHealthService->consumingMsgFailed = consumingMsgFailed;
        scorerHealthService->consumerReadMessage = consumerReadMessage;
        scorerHealthService->endOfPartitionReached = endOfPartitionReached;
        scorerHealthService->entityToModuleStateStats =beanFactory->entityToModuleStateStats.get();
        scorerHealthService->numberOfExceptionsInScorerMessageProcessor = numberOfExceptionsInScorerMessageProcessor;
        scorerHealthService->scorerHealthFlag = scorerHealthFlag;


        auto directLastTimeSeenSource = std::make_unique<LastTimeSeenSource>(
                beanFactory->aeroSpikeDriver.get(),
                "rtb",
                "usersSeenSet",
                "scoredInLastN"
                );

        auto scoredInLastNMinutesModule = std::make_shared<ScoredInLastNMinutesModule>(
                beanFactory->configService->getAsInt("intervalOfScoringADeviceInSeconds")
                );
        scoredInLastNMinutesModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        scoredInLastNMinutesModule->directLastTimeSeenSource = std::move(directLastTimeSeenSource);



        scoringMessageProcessor->scoredInLastNMinutesModule = scoredInLastNMinutesModule;
        scoringMessageProcessor->deviceSegmentHistoryCassandraService = beanFactory->deviceSegmentHistoryCassandraService.get();
        scoringMessageProcessor->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        scoringMessageProcessor->segmentToDeviceCassandraService =  beanFactory->segmentToDeviceCassandraService.get();
        scoringMessageProcessor->calculateScoreModule = calculateScoreModule;
        scoringMessageProcessor->segmentCreatorModule = segmentCreatorModule;
        scoringMessageProcessor->deviceSegmentPersistenceModule = deviceSegmentPersistenceModule;
        scoringMessageProcessor->crossWalkDeviceSegmentPersistenceModule = crossWalkDeviceSegmentPersistenceModule;
        scoringMessageProcessor->segmentToDevicePersistenceModule = segmentToDevicePersistenceModule;
        scoringMessageProcessor->deviceFeatureFetcherModule = deviceFeatureFetcherModule;
        scoringMessageProcessor->allLoadedModels = allLoadedModels;
        scoringMessageProcessor->isReadyToProcessFlag = isMessageProcessorReadyToProcess;
        scoringMessageProcessor->configService = beanFactory->configService.get();
        scoringMessageProcessor->numberOfExceptionsInScorerMessageProcessor = numberOfExceptionsInScorerMessageProcessor;
        scoringMessageProcessor->scorerHealthService = scorerHealthService;

        scoringMessageProcessor->setTheModuleList();


        auto scorerAsyncJobsService = std::make_shared<ScorerAsyncJobsService>();
        scorerAsyncJobsService->modelIdToModelScoreLevelMap = modelIdToModelScoreLevelMap;


        scorerAsyncJobsService->entityToModuleStateStatsPersistenceService =
                beanFactory->entityToModuleStateStatsPersistenceService.get();
        scorerAsyncJobsService->configService = beanFactory->configService.get();
        scorerAsyncJobsService->isMetricPersistenceInProgress = std::make_shared<gicapods::AtomicBoolean>();
        scorerAsyncJobsService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        scorerAsyncJobsService->modelResultCacheService = beanFactory->modelResultCacheService.get();
        scorerAsyncJobsService->scorerHealthService = scorerHealthService;
        scorerAsyncJobsService->allLoadedModels = allLoadedModels;
        scorerAsyncJobsService->dataReloadService = serviceFactory->dataReloadService;

        scorerAsyncJobsService->refreshCachesIntervalInSeconds = 300;
        scorerAsyncJobsService->isMessageProcessorReadyToProcess = isMessageProcessorReadyToProcess;
        scorerAsyncJobsService->startAsyncThreads();

        auto scoringKafkaCluster = beanFactory->configService->get("scoringKafkaCluster");
        auto scoringTopicName = beanFactory->configService->get("scoringTopicName");

        int partitionNumber = 0; //configService->get("partitionNumber", partitionNumber); TODO : make this property driven
        int64_t startOffset = RdKafka::Topic::OFFSET_STORED;
        auto kafkaConsumer = std::make_shared<KafkaConsumer>(scoringKafkaCluster,
                                                             scoringTopicName,
                                                             partitionNumber,
                                                             scoringMessageProcessor,
                                                             startOffset);

        kafkaConsumer->consumerTimeout = consumerTimeout;
        kafkaConsumer->lastOffsetRead = lastOffsetRead;
        kafkaConsumer->consumingMsgFailed = consumingMsgFailed;
        kafkaConsumer->consumerReadMessage = consumerReadMessage;
        kafkaConsumer->endOfPartitionReached = endOfPartitionReached;
        kafkaConsumer->scorerHealthFlag = scorerHealthFlag;
        kafkaConsumer->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();


        std::vector<std::string> vecTopics;
        std::vector<std::string> hosts;
        hosts.push_back(scoringKafkaCluster);
        vecTopics.push_back(scoringTopicName);

        std::shared_ptr<KafkaSubscriber> subscriber  = std::make_shared<KafkaSubscriber>(scoringKafkaCluster.c_str(),
                                                                                         "scoring1",
                                                                                         vecTopics,
                                                                                         beanFactory->configService->getAsBooleanFromString("startFromBeginningOfAllPartitions"),
                                                                                         scoringMessageProcessor);

        subscriber->connect();
        subscriber->consumerTimeout = consumerTimeout;
        subscriber->lastOffsetRead = lastOffsetRead;
        subscriber->consumingMsgFailed = consumingMsgFailed;
        subscriber->consumerReadMessage = consumerReadMessage;
        subscriber->endOfPartitionReached = endOfPartitionReached;
        subscriber->scorerHealthFlag = scorerHealthFlag;
        subscriber->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        subscriber->consumeMSG();

        beanFactory->cassandraDriver->closeSessionAndCluster();
}
