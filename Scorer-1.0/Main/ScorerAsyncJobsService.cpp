#include "GUtil.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include "ScorerAsyncJobsService.h"
#include "BeanFactory.h"
#include "JsonMapUtil.h"

#include "EntityToModuleStateStatsPersistenceService.h"
#include "MySqlSegmentService.h"
#include "ModelCacheService.h"
#include "FileUtil.h"
#include "JsonUtil.h"
#include "HttpUtil.h"
#include "ModelResultCacheService.h"
#include "DateTimeUtil.h"
ScorerAsyncJobsService::ScorerAsyncJobsService() {
        lastTimeDataWasReloadedInSeconds = DateTimeUtil::getNowInSecond();

}

ScorerAsyncJobsService::~ScorerAsyncJobsService() {

}

void ScorerAsyncJobsService::refreshFiles(std::string fileName) {
        // std::string fileName = "/tmp/top_common_devices";
        std::string fileFullName = "/tmp/" + fileName + ".txt";
        if (!FileUtil::checkIfFileExists(fileFullName)
            || DateTimeUtil::getNowInSecond() - FileUtil::getLastModifiedInSeconds(fileFullName) > 600) {
                LOG(INFO) << "downling file from server : "<< fileFullName;
                std::string url = "scp://67.205.170.169/tmp/" + fileName + "/part-00000";
                if (FileUtil::checkIfFileExists(fileFullName)) {
                        FileUtil::deleteFile(fileFullName);
                }
                HttpUtil::downloadFileUsingScpWithCurl(url, fileFullName);
        }

        std::string modelDistributionsInJson = FileUtil::readFileInString(fileFullName);

        /*
           {"5":[85.81,143.43750000000003,201.06500000000005,258.69250000000005,316.32000000000005,373.9475000000001,431.5750000000001,489.20250000000016,546.8300000000002],"7":[0.0,6.7375,13.475,20.2125,26.95,33.6875,40.425,47.1625,53.9],"8":[0.0,8.05125,16.1025,24.15375,32.205,40.256249999999994,48.3075,56.35875,64.41],"9":[6.64,23.24,39.839999999999996,56.44,73.03999999999999,89.63999999999999,106.24,122.83999999999999,139.43999999999997]}
         */
        readMapFromJsonString(modelDistributionsInJson);
}

void ScorerAsyncJobsService::readMapFromJsonString(std::string jsonString) {

        auto valueDocument = parseJsonSafely(jsonString);

        for (auto itr = valueDocument->MemberBegin();
             itr != valueDocument->MemberEnd(); ++itr)
        {


                int modelId = ConverterUtil::TO <std::string, int>(itr->name.GetString());
                auto container = std::make_shared<ModelScoreLevelContainer>();

                RapidJsonValueType arrayValue = itr->value;
                double lastValue = 0;
                double levelPercentage = 100/arrayValue.Size ();
                for (rapidjson::SizeType i = 0; i < arrayValue.Size (); i++) {
                        RapidJsonValueType oneValue = (arrayValue)[i];
                        double beginningOfWindow = oneValue.GetDouble ();
                        std::shared_ptr<ModelScoreLevel> modelScoreLevel =
                                std::make_shared<ModelScoreLevel> ();
                        modelScoreLevel->minScoreOfLevel = beginningOfWindow;
                        double endOfWindow = beginningOfWindow * 100;
                        if ( i + 1 < arrayValue.Size()) {
                                //next value exists
                                RapidJsonValueType nextValue = (arrayValue)[i+1];
                                endOfWindow = nextValue.GetDouble();
                                //this is to make sure array is sorted as expected
                                assertAndThrow(endOfWindow >= beginningOfWindow);
                        }
                        modelScoreLevel->maxScoreOfLevel = endOfWindow;
                        modelScoreLevel->levelName = _toStr(levelPercentage*(i+1))+"%";
                        container->modelScoreLevels->push_back(modelScoreLevel);
                        lastValue = beginningOfWindow;
                }


                modelIdToModelScoreLevelMap->put(modelId, container);
        }

        LOG(ERROR)<<"modelIdToModelScoreLevelMap : "<<
                JsonMapUtil::convertMapToJsonArray (*modelIdToModelScoreLevelMap);

}

void ScorerAsyncJobsService::runEveryHour() {
        while (true) {
                try {
                        refreshFiles("model_score_distribution_last7days");

                        entityToModuleStateStats->addStateModuleForEntity ("runEveryHour",
                                                                           "ScorerAsyncJobsService",
                                                                           "ALL");



                        gicapods::Util::printMemoryUsage();
                } catch (std::exception const &e) {
                        gicapods::Util::showStackTrace();
                        //TODO : go red and show in dashboard that this is red
                        LOG(ERROR) << "error happening when runEveryHour  " << boost::diagnostic_information (e);
                }

                gicapods::Util::sleepViaBoost (_L_, 60 * 60);
        }

}

void ScorerAsyncJobsService::runEveryMinute() {
        while (true) {
                try {

                        entityToModuleStateStats->addStateModuleForEntity ("runEveryMinute",
                                                                           "ScorerAsyncJobsService",
                                                                           "ALL");

                        scorerHealthService->determineHealth();
                        gicapods::Util::printMemoryUsage();
                        int oldnessOfReloadedData = DateTimeUtil::getNowInSecond() - lastTimeDataWasReloadedInSeconds;
                        if (oldnessOfReloadedData > 300) {
                                entityToModuleStateStats->addStateModuleForEntity(
                                        "DATA_IS_TOO_OLD",
                                        "ActionRecorderAsyncJobService",
                                        EntityToModuleStateStats::all,
                                        EntityToModuleStateStats::exception
                                        );
                        }
                } catch (std::exception const &e) {
                        gicapods::Util::showStackTrace();
                        //TODO : go red and show in dashboard that this is red
                        LOG(ERROR) << "error happening when runEveryMinute  " << boost::diagnostic_information (e);
                }  catch (...) {
                        gicapods::Util::showStackTrace();

                }

                gicapods::Util::sleepViaBoost (_L_, 60);
        }
}

void ScorerAsyncJobsService::refreshData() {
        while (true) {
                try {
                        entityToModuleStateStats->addStateModuleForEntity ("refreshData",
                                                                           "ScorerAsyncJobsService",
                                                                           "ALL");

                        dataReloadService->reloadDataViaHttp();

                        auto allModels = dataReloadService->beanFactory->modelCacheService->getAllEntities();

                        if (allModels->empty()) {
                                entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION_NO_MODEL_RECEIVED_FROM_DATAMASTER",
                                                                                   "ScorerAsyncJobsService",
                                                                                   "ALL");
                        }
                        LOG(INFO)<< "loaded "<<allModels->size() << " models for initial consideration";
                        isMessageProcessorReadyToProcess->setValue(false);
                        //wait 3 seconds before clearing the models
                        gicapods::Util::sleepViaBoost (_L_, 3);
                        allLoadedModels->clear();


                        for (std::shared_ptr<ModelRequest> model : *allModels) {

                                auto modelResult = modelResultCacheService->
                                                   modelRequestIdToResultMap->getOptional(model->id);

                                if (modelResult != nullptr) {

                                        model->modelResult = modelResult;
                                        MLOG(2)<<" found result for model : "
                                               <<model->toJson()
                                               <<" \n"<<" modelResult: "
                                               << modelResult->toJson();
                                }

                                tbb::concurrent_hash_map<int, std::shared_ptr<ModelRequest> >::accessor accessor;
                                //if a model doesn't have topFeatureScoreMap or featureScoreMap
                                // means its not built yet, or something is wrong with it
                                // so we can't use it for scoring
                                if (model->modelResult->topFeatureScoreMap.empty()) {
                                        entityToModuleStateStats->addStateModuleForEntity ("MODEL_NOT_ELIGIBLE_FOR_SCORING",
                                                                                           "ScorerAsyncJobsService",
                                                                                           "mdl" + StringUtil::toStr(model->id));
                                } else {
                                        entityToModuleStateStats->addStateModuleForEntity ("MODEL_ELIGIBLE_FOR_SCORING",
                                                                                           "ScorerAsyncJobsService",
                                                                                           "mdl" + StringUtil::toStr(model->id));
                                        allLoadedModels->insert(accessor, model->id);
                                        accessor->second = model;
                                }

                        }
                        if (!allLoadedModels->empty()) {
                                isMessageProcessorReadyToProcess->setValue(true);
                        } else {
                                LOG(ERROR) << "NO MODEL WAS LOADED, Message Processor is not Ready";
                        }
                        MLOG(3) << "read  " << allModels->size () << " active models for all advertisers";
                        lastTimeDataWasReloadedInSeconds = DateTimeUtil::getNowInSecond();
                } catch (std::exception const &e) {
                        gicapods::Util::showStackTrace();
                        //TODO : go red and show in dashboard that this is red
                        LOG(ERROR) << "error happening when runEveryMinute  " << boost::diagnostic_information (e);
                }  catch (...) {
                        gicapods::Util::showStackTrace();

                }

                gicapods::Util::sleepViaBoost (_L_, refreshCachesIntervalInSeconds);
        }
}

std::string ScorerAsyncJobsService::getName() {
        return "ScorerAsyncJobsService";
}

void ScorerAsyncJobsService::startAsyncThreads() {


        std::thread everyMinuteThread (&ScorerAsyncJobsService::runEveryMinute, this);
        everyMinuteThread.detach();

        std::thread everyHourThread (&ScorerAsyncJobsService::runEveryHour, this);
        everyHourThread.detach();

        std::thread refreshDataThread(&ScorerAsyncJobsService::refreshData, this);
        refreshDataThread.detach();

        this->entityToModuleStateStatsPersistenceService->startThread();

}
