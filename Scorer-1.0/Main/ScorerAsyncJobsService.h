#ifndef bidderAsyncJobsService_H_
#define bidderAsyncJobsService_H_


#include <memory>
#include <string>
class EntityToModuleStateStats;

#include "DataReloadService.h"
#include "ModelScoreLevel.h"
#include "ModelScoreLevelContainer.h"
namespace gicapods { class ConfigService; }

#include <boost/thread.hpp>
#include <thread>
#include "AtomicLong.h"
#include "ScorerHealthService.h"
#include "MySqlAdvertiserModelMappingService.h"
#include "DateTimeMacro.h"
class ModelCacheService;
class ModelResultCacheService;
class ModelRequest;
#include "ConcurrentHashMap.h"
class ScorerAsyncJobsService;
class EntityToModuleStateStatsPersistenceService;



class ScorerAsyncJobsService : public std::enable_shared_from_this<ScorerAsyncJobsService> {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<int, ModelScoreLevelContainer > > modelIdToModelScoreLevelMap;
int refreshCachesIntervalInSeconds;
TimeType lastTimeDataWasReloadedInSeconds;

gicapods::ConfigService* configService;
std::shared_ptr<gicapods::AtomicBoolean> isMetricPersistenceInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isMessageProcessorReadyToProcess;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;

ModelResultCacheService* modelResultCacheService;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<ScorerHealthService> scorerHealthService;
std::shared_ptr<DataReloadService> dataReloadService;
std::shared_ptr<tbb::concurrent_hash_map<int, std::shared_ptr<ModelRequest> > > allLoadedModels;

ScorerAsyncJobsService();

virtual ~ScorerAsyncJobsService();

void runEveryMinute();
void readMapFromJsonString(
        std::string jsonString);
void refreshFiles(std::string fileName);

void runEveryHour();

std::string getName();

void startAsyncThreads();

void refreshData();

private:
bool _helpRequested;
};

#endif
