
#include "GUtil.h"
#include "Status.h"
#include "StringUtil.h"
#include "JsonUtil.h"
#include "ScorerContext.h"
#include "Segment.h"

#include "ModelRequest.h"

std::shared_ptr<gicapods::AtomicBoolean> ScorerContext::getScorerInterruptedFlag() {
        static std::shared_ptr<gicapods::AtomicBoolean> srv = std::make_shared<gicapods::AtomicBoolean>();
        return srv;
}

ScorerContext::ScorerContext() {
        status = std::make_shared<gicapods::Status>();
        modelIdToScoreMap = std::make_shared<tbb::concurrent_hash_map<int, double> > ();
        allSegmentsCreatedForDevice = std::make_shared<std::vector<std::shared_ptr<Segment>> > ();
}

std::string ScorerContext::toString() {
        return this->toJson();
}

std::string ScorerContext::toJson() {

        std::string json;

        DocumentPtr doc = std::make_shared<DocumentType>();
        doc->SetObject();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        return JsonUtil::valueToString(value);
}

ScorerContext::~ScorerContext() {

}
