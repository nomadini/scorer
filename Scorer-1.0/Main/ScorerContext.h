#ifndef ScorerContext_H
#define ScorerContext_H



#include "Status.h"
#include "AtomicBoolean.h"
#include <memory>
#include <string>
#include <vector>
class DeviceFeatureHistory;
class ModelRequest;
class Segment;
#include "TransAppMessage.h"
#include <tbb/concurrent_hash_map.h>

class ScorerContext {

private:

public:

std::shared_ptr<gicapods::Status> status;
std::shared_ptr<TransAppMessage> transAppMessage;
std::shared_ptr<tbb::concurrent_hash_map<int, std::shared_ptr<ModelRequest> > >  allLoadedModels;
std::shared_ptr<tbb::concurrent_hash_map<int, double> > modelIdToScoreMap;
std::shared_ptr<std::vector<std::shared_ptr<Segment> > > allSegmentsCreatedForDevice;
ScorerContext();

std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory;

std::string lastModuleRunInPipeline;
static std::shared_ptr<gicapods::AtomicBoolean> getScorerInterruptedFlag();

std::string toString();

std::string toJson();

virtual ~ScorerContext();
};

#endif
