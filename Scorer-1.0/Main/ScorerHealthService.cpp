

#include "ScorerHealthService.h"
#include "GUtil.h"

ScorerHealthService::ScorerHealthService() {

}

void ScorerHealthService::determineHealth() {

        LOG_EVERY_N(ERROR, 1000)<<std::endl
                                <<" consumerTimeout : " << consumerTimeout->getValue() << std::endl
                                <<" consumingMsgFailed : " << consumingMsgFailed->getValue() << std::endl
                                <<" consumerReadMessage : " << consumerReadMessage->getValue() << std::endl
                                <<" numberOfExceptionsInScorerMessageProcessor : " << numberOfExceptionsInScorerMessageProcessor->getValue() << std::endl
                                <<" lastOffsetRead : " << lastOffsetRead->getValue() << std::endl
                                <<" endOfPartitionReached : " << endOfPartitionReached->getValue() << std::endl;



        if (numberOfExceptionsInScorerMessageProcessor->getValue() > numberOfExceptionsInScorerMessageProcessorLimit->getValue()) {
                LOG_EVERY_N(ERROR, 1)<<"too many exceptions : " <<numberOfExceptionsInScorerMessageProcessor->getValue()<<
                        " stopping consumer now";
                scorerHealthFlag->setValue(false);
        } else {
                scorerHealthFlag->setValue(true);
        }
        // if (queueOfContexts->unsafe_size() > 1000 ) {
        //         LOG_EVERY_N(ERROR, 1000) << "size of async job queue is too Large: "<< queueOfContexts->unsafe_size();
        //         std::string reason = "AsyncPipelineIsSlow queueSize : :" + StringUtil::toStr(queueOfContexts->unsafe_size());
        //         reasons.push_back(reason);
        //         bidMode->setStatus("NO_BID", "ScorerHealthService",reason);
        // }
        //
        // LOG_EVERY_N(ERROR, 1000) <<" kafka queueSizeWaitingForAck : " << queueSizeWaitingForAck->getValue() << std::endl;
        //
        //
        // if (queueSizeWaitingForAck->getValue() > 1000) {
        //         //         std::string reason = "EnqueueForScoringModule_queueSizeWaitingForAck:" + StringUtil::toStr(queueSizeWaitingForAck->getValue());
        //         //         reasons.push_back(reason);
        //         //         bidMode->setStatus("NO_BID", "ScorerHealthService",reason);
        //         LOG_EVERY_N(ERROR, 1000) << "kafka queueSizeWaitingForAck : " << queueSizeWaitingForAck->getValue();
        // }
        //
        // //set to DO_BID, if none of NO_BID conditions are true
        // if (!StringUtil::equalsIgnoreCase(bidMode->getStatus(), "NO_BID")) {
        //         bidMode->setStatus("DO_BID", "ScorerHealthService", "ALL_IN_GOOD_CONDITIONS_BEFORE_EVALUATION");
        // } else {
        //         std::string allReasons = JsonArrayUtil::convertListToJson(reasons);
        //         bidMode->setStatus("NO_BID", "ScorerHealthService", allReasons);
        //         LOG_EVERY_N(ERROR, 1000) << "bidder is NO_BID_MODE, reasons : " << allReasons;
        // }
        //
        //
        // return bidMode;
}
