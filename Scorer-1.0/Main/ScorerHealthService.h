#ifndef ScorerHealthService_H
#define ScorerHealthService_H

#include "AtomicBoolean.h"
#include "AtomicLong.h"
#include <tbb/concurrent_queue.h>
class EntityToModuleStateStats;

class ScorerHealthService {


public:
    EntityToModuleStateStats* entityToModuleStateStats;

    std::shared_ptr<gicapods::AtomicLong> consumerTimeout;
    std::shared_ptr<gicapods::AtomicLong> consumingMsgFailed;
    std::shared_ptr<gicapods::AtomicLong> consumerReadMessage;
    std::shared_ptr<gicapods::AtomicLong> endOfPartitionReached;
    std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInScorerMessageProcessor;
    std::shared_ptr<gicapods::AtomicBoolean> scorerHealthFlag;
    std::shared_ptr<gicapods::AtomicLong> lastOffsetRead;
    std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInScorerMessageProcessorLimit;
    ScorerHealthService();
    void determineHealth();

};



#endif
