#include "GUtil.h"
#include "SvmSgdScorer.h"
#include "ModelRequest.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "KafkaConsumer.h"
#include "KafkaProducer.h"
#include "ScoringMessageProcessor.h"
#include "DeviceFeatureHistoryTestHelper.h"
#include "ConfigService.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "GalaxieSetup.h"
#include <boost/thread/thread.hpp>
#include "GalaxieSetup.h"
#include "Feature.h"

SvmSgdScorer::SvmSgdScorer(
        DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService) {
        this->deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
}

double SvmSgdScorer::calculateTheScoreOfBrowserBasedOnModel(
        std::set<std::string> features, std::shared_ptr<ModelRequest> model) {

        double sumOfFeaturesScores = 0;

        for (std::set<std::string>::iterator featureIter = features.begin();
             featureIter != features.end(); ++featureIter) {

                auto feaureScorePair = model->modelResult->featureScoreMap.find(*featureIter);
                if (feaureScorePair != model->modelResult->featureScoreMap.end()) {

                        MLOG(3)<<"feature  "<<feaureScorePair->first<<" was found with score  "<<feaureScorePair->second;

                        sumOfFeaturesScores += feaureScorePair->second;
                } else {
                        LOG(WARNING)<<"feature  "<<*featureIter<<" was not found to score ";
                }

        } //end of summing up the feature scores for an offerId
        if (sumOfFeaturesScores != 0) {
                MLOG(3)<<"sumOfFeaturesScores : "<<sumOfFeaturesScores;
        }
        return sumOfFeaturesScores;

}
