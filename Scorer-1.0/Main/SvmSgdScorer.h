/*
 * SvmSgdScorer.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef SVMSGDSCORER_H_
#define SVMSGDSCORER_H_

#include "DeviceFeatureHistoryCassandraService.h"
#include "ModelRequest.h"
#include "ModelUtil.h"
#include "ModelRequest.h"

#include <memory>
#include <string>
#include <set>
class SvmSgdScorer {

public:

DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
SvmSgdScorer(DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService);

//this score calculation only counts the features with positive scores
static double calculateTheScoreOfBrowserBasedOnModel(
        std::set<std::string> features, std::shared_ptr<ModelRequest> model);
};



#endif /* SVMSGDSCORER_H_ */
