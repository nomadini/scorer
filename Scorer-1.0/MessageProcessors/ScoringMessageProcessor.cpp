
#include "ScoringMessageProcessor.h"
#include "ScoreToSegmentConverter.h"
#include "SegmentCreatorModule.h"

#include "SegmentToDevicePersistenceModule.h"
#include "DeviceSegmentPersistenceModule.h"

#include "StringUtil.h"
#include "CalculateScoreModule.h"
#include "Status.h"
#include "SegmentToDeviceCassandraService.h"
#include "TransAppMessage.h"
#include "ScorerHealthService.h"

#include "EntityToModuleStateStats.h"
#include "ScorerContext.h"
#include "DeviceSegmentHistoryCassandraService.h"
#include "SegmentToDeviceCassandraService.h"
#include "DeviceSegmentPersistenceModule.h"
#include <tbb/concurrent_hash_map.h>
#include "MySqlAdvertiserModelMappingService.h"
#include "ModelCacheService.h"
#include "ScorerHealthService.h"
#include "AtomicBoolean.h"
#include "BeanFactory.h"

ScoringMessageProcessor::ScoringMessageProcessor() {

}

ScoringMessageProcessor::~ScoringMessageProcessor() {

}

void ScoringMessageProcessor::setTheModuleList() {
								this->modules.push_back(scoredInLastNMinutesModule);
								this->modules.push_back(deviceFeatureFetcherModule);
								this->modules.push_back(calculateScoreModule);
								this->modules.push_back(segmentCreatorModule);
								this->modules.push_back(deviceSegmentPersistenceModule);
								this->modules.push_back(crossWalkDeviceSegmentPersistenceModule);
								this->modules.push_back(segmentToDevicePersistenceModule);
}


bool ScoringMessageProcessor::isReadyToProcess()  {
								return isReadyToProcessFlag->getValue();
}
void ScoringMessageProcessor::process(std::string msg)  {

								auto transAppMessage = TransAppMessage::fromJson(msg);
								auto context = std::make_shared<ScorerContext>();
								context->transAppMessage = transAppMessage;
								context->allLoadedModels = allLoadedModels;
								process(context);
}

std::string ScoringMessageProcessor::getName() {
								return "ScoringMessageProcessor";
}

void ScoringMessageProcessor::process(std::shared_ptr<ScorerContext> context) {

								scorerHealthService->determineHealth();
								entityToModuleStateStats->addStateModuleForEntity("PROCESSING_MESSAGE","ScoringMessageProcessor","ALL");
								if (context->allLoadedModels->empty()) {
																throwEx("no models to build score");
								}
								for (auto module : modules) {

																if (context->status->value != STOP_PROCESSING) {
																								try {
																																module->process(context);
																								} catch(...) {
																																numberOfExceptionsInScorerMessageProcessor->increment();
																																entityToModuleStateStats->addStateModuleForEntity(
																																								"PROCESSOR_EXCEPTION",
																																								"ScoringMessageProcessor",
																																								EntityToModuleStateStats::all,
																																								EntityToModuleStateStats::exception);
																																context->status->value = STOP_PROCESSING;
																																gicapods::Util::showStackTrace();
																								}
																} else {
																								entityToModuleStateStats->
																								addStateModuleForEntity(
																																"LastModule-" + module->getName (),
																																"ScoringMessageProcessor",
																																"ALL");
																								break;
																}

								}

}
