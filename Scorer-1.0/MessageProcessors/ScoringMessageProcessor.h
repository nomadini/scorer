/*
 * ScoringMessageProcessor.h
 *
 *  Created on: Aug 1, 2015
 *      Author: mtaabodi
 */

#ifndef ScoringMessageProcessor_H
#define ScoringMessageProcessor_H



#include "ScorerModule.h"
#include <memory>
#include <string>
#include "AtomicLong.h"
#include "MessageProcessor.h"


class ScorerContext;
class DeviceSegmentHistoryCassandraService;
class SegmentToDeviceCassandraService;
class DeviceSegmentPersistenceModule;
class CrossWalkDeviceSegmentPersistenceModule;
#include <tbb/concurrent_hash_map.h>
class MySqlAdvertiserModelMappingService;
class ModelCacheService;
class ScorerHealthService;
class BeanFactory;
#include "AtomicBoolean.h"

namespace gicapods { class ConfigService; }

class EntityToModuleStateStats;


class ScoringMessageProcessor;
class ModelRequest;


class ScoringMessageProcessor : public MessageProcessor {

public:

std::vector<std::shared_ptr<ScorerModule> > modules;
gicapods::ConfigService* configService;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInScorerMessageProcessor;

DeviceSegmentHistoryCassandraService* deviceSegmentHistoryCassandraService;
SegmentToDeviceCassandraService* segmentToDeviceCassandraService;
std::unique_ptr<BeanFactory> beanFactory;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::AtomicBoolean> isReadyToProcessFlag;
std::shared_ptr<ScorerModule> deviceFeatureFetcherModule;
std::shared_ptr<ScorerModule> calculateScoreModule;
std::shared_ptr<ScorerModule> segmentCreatorModule;
std::shared_ptr<ScorerModule> deviceSegmentPersistenceModule;
std::shared_ptr<ScorerModule> crossWalkDeviceSegmentPersistenceModule;
std::shared_ptr<ScorerModule> segmentToDevicePersistenceModule;
std::shared_ptr<ScorerModule> scoredInLastNMinutesModule;
std::shared_ptr<ScorerHealthService> scorerHealthService;
std::shared_ptr<tbb::concurrent_hash_map<int, std::shared_ptr<ModelRequest> > > allLoadedModels;

ScoringMessageProcessor();

void setTheModuleList();

std::string getName();
void process(std::shared_ptr<ScorerContext> opt);
void process(std::string msg);
bool isReadyToProcess();

virtual ~ScoringMessageProcessor();

};



#endif /* GICAPODS_GICAPODSSERVER_SRC_KAFKA_MESSAGEPROCESSOR_H_ */
