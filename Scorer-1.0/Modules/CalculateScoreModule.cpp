
#include "CalculateScoreModule.h"
#include "EntityToModuleStateStats.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "DeviceFeatureHistory.h"
#include "Device.h"
#include "ModelRequest.h"
#include <boost/foreach.hpp>


CalculateScoreModule::CalculateScoreModule() {

}

void CalculateScoreModule::beforeProcessing(std::shared_ptr<ScorerContext> context) {

}

void CalculateScoreModule::afterProcessing(std::shared_ptr<ScorerContext> context) {

}

std::string CalculateScoreModule::getName() {
								return "CalculateScoreModule";
}

void CalculateScoreModule::process(std::shared_ptr<ScorerContext> context) {

								for(auto&& modelPair : *context->allLoadedModels) {
																std::string modelId = StringUtil::toStr("mdl")+ StringUtil::toStr(modelPair.second->id);
																entityToModuleStateStats->addStateModuleForEntity(
																								"SCORING_MODEL_FOR_MODEL:" + modelId,
																								"CalculateScoreModule",
																								EntityToModuleStateStats::all);
																double scoreOfModel = calculateScoreForModel(context, modelPair.second);
																// if (scoreOfModel != 0) { //TODO : uncomment later
																tbb::concurrent_hash_map<int, double>::accessor accessor;
																context->modelIdToScoreMap->insert(accessor, modelPair.second->id);
																accessor->second = scoreOfModel;
																// }


								}


								context->status->value = CONTINUE_PROCESSING;;


}

std::unordered_map<std::string, std::shared_ptr<Feature> > CalculateScoreModule::getFeaturesAsMap(
								std::vector<std::shared_ptr<FeatureHistory> > features) {
								std::unordered_map<std::string, std::shared_ptr<Feature> > featureMap;
								for (auto feature : features) {
																featureMap.insert(std::make_pair(feature->feature->getName(), feature->feature));
								}
								return featureMap;
}

double CalculateScoreModule::calculateScoreForModel(std::shared_ptr<ScorerContext> context, std::shared_ptr<ModelRequest> model) {

								double sumOfScore = 0;

								typedef std::unordered_map<std::string, double> map_t;
								auto featureMap = getFeaturesAsMap(*context->deviceFeatureHistory->getFeatures());
								for(map_t::value_type &entry :  model->modelResult->topFeatureScoreMap)
								{
																double scoreOfTopFeature = model->modelResult->getScoreOfTopFeatures(entry.first);
																if (scoreOfTopFeature != 0 &&
																				featureMap.find(entry.first) !=
																				featureMap.end()) {
																								MLOG(3)<<"feature found and score is gt than zero, adding to sum, "
																																" feature :  "<<entry.first
																															<<", score : "<<scoreOfTopFeature;

																								sumOfScore += scoreOfTopFeature;
																}
								}
								LOG_EVERY_N(INFO, 10000)<<google::COUNTER<<"th deviceId :  "<<context->deviceFeatureHistory->device->getDeviceId()<<
																", deviceType :  "<< context->deviceFeatureHistory->device->getDeviceType()<<
																", sumOfScore :"<<sumOfScore;

								if (sumOfScore > 0) {
																entityToModuleStateStats->addStateModuleForEntity(
																								"SCORING_DEVICE_ID_SUCCESS",
																								"CalculateScoreModule",
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::important);
								}
								return sumOfScore;

}
CalculateScoreModule::~CalculateScoreModule() {
}
