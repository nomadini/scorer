#ifndef CalculateScoreModule_H
#define CalculateScoreModule_H


#include "Status.h" //this is needed in every module
#include "ScorerContext.h"
#include <memory>
#include <string>
#include <unordered_map>
#include "FeatureHistory.h"
#include "Feature.h"
#include "ScorerModule.h"
class EntityToModuleStateStats;
class ModelRequest;
class CalculateScoreModule;


class CalculateScoreModule : public ScorerModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;

CalculateScoreModule();

void beforeProcessing(std::shared_ptr<ScorerContext> context);

void afterProcessing(std::shared_ptr<ScorerContext> context);

double calculateScoreForModel(std::shared_ptr<ScorerContext> context,
																														std::shared_ptr<ModelRequest> model);

virtual void process(std::shared_ptr<ScorerContext> context);

std::unordered_map<std::string, std::shared_ptr<Feature> > getFeaturesAsMap(std::vector<std::shared_ptr<FeatureHistory> > features);
virtual std::string getName();

virtual ~CalculateScoreModule();
};



#endif
