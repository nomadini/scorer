

#include "DeviceFeatureFetcherModule.h"
#include "EntityToModuleStateStats.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "NumberUtil.h"
#include "Feature.h"
#include "StringUtil.h"
#include "DeviceFeatureHistory.h"

DeviceFeatureFetcherModule::DeviceFeatureFetcherModule() {
        totalFeatures = std::make_shared<gicapods::AtomicLong>();
        totalDevices = std::make_shared<gicapods::AtomicLong>();

        featureRecencyInSecond = (TimeType) (3600 * 24 * 2); //2 days in seconds
}


void DeviceFeatureFetcherModule::beforeProcessing(std::shared_ptr<ScorerContext> context) {

}

void DeviceFeatureFetcherModule::afterProcessing(std::shared_ptr<ScorerContext> context) {

}
void DeviceFeatureFetcherModule::process(std::shared_ptr<ScorerContext> context) {

        context->deviceFeatureHistory = deviceFeatureHistoryCassandraService->
                                        readFeatureHistoryOfDevice(context->transAppMessage->device,
                                                                   featureRecencyInSecond,
                                                                   Feature::getAllFeatureTypes(),
                                                                   1000);
        if (context->deviceFeatureHistory->getFeatures()->empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("NO_FEATURE_HISTORY_FOR_DEVICE",
                                                                   "DeviceFeatureFetcherModule",
                                                                   "ALL");
                context->status->value = STOP_PROCESSING;
                return;
        }

        totalFeatures->addValue(context->deviceFeatureHistory->getFeatures()->size());
        totalDevices->increment();

        if (totalDevices->getValue() % 10000 == 1) {
                int avgFeatureSizeForDevice =
                        totalFeatures->getValue() / totalDevices->getValue();

                entityToModuleStateStats->addStateModuleForEntity (
                        "size=" + StringUtil::toStr(
                                NumberUtil::getNearestMultipleToNumber(avgFeatureSizeForDevice, 20)),
                        "DeviceFeatureFetcherModule",
                        "AVG_FEATURE_HISTORY_SIZE_FOR_DEVICE");
                totalDevices->setValue(0);
                totalFeatures->setValue(0);
        }


}

std::string DeviceFeatureFetcherModule::getName() {
        return "DeviceFeatureFetcherModule";
}


DeviceFeatureFetcherModule::~DeviceFeatureFetcherModule() {

}
