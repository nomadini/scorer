

#ifndef DeviceFeatureFetcherModule_H
#define DeviceFeatureFetcherModule_H


#include "Status.h" //this is needed in every module
#include "ScorerContext.h"
#include <memory>
#include <string>
#include "ScorerModule.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "AtomicLong.h"
class EntityToModuleStateStats;

class DeviceFeatureFetcherModule : public ScorerModule {

public:
EntityToModuleStateStats* entityToModuleStateStats;

DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
TimeType featureRecencyInSecond;

std::shared_ptr<gicapods::AtomicLong> totalFeatures;
std::shared_ptr<gicapods::AtomicLong> totalDevices;

DeviceFeatureFetcherModule();

void beforeProcessing(std::shared_ptr<ScorerContext> context);

void afterProcessing(std::shared_ptr<ScorerContext> context);

virtual void process(std::shared_ptr<ScorerContext> context);

virtual std::string getName();

virtual ~DeviceFeatureFetcherModule();
};

#endif
