
#include "ModelScoreLevel.h"
#include "JsonUtil.h"

#include <boost/foreach.hpp>

ModelScoreLevel::ModelScoreLevel() {
        minScoreOfLevel = 0;
        maxScoreOfLevel = 0;

}


ModelScoreLevel::~ModelScoreLevel() {
}

std::shared_ptr<ModelScoreLevel> ModelScoreLevel::fromJson(std::string jsonString) {
        auto document = parseJsonSafely(jsonString);
        return ModelScoreLevel::fromJsonValue(*document);
}

void ModelScoreLevel::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair(doc, "minScoreOfLevel", minScoreOfLevel, value);
        JsonUtil::addMemberToValue_FromPair(doc, "maxScoreOfLevel", maxScoreOfLevel, value);
        JsonUtil::addMemberToValue_FromPair(doc, "levelName", levelName, value);
}

std::shared_ptr<ModelScoreLevel> ModelScoreLevel::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<ModelScoreLevel> obj = std::make_shared<ModelScoreLevel>();
        obj->minScoreOfLevel = JsonUtil::GetDoubleSafely (value, "minScoreOfLevel");
        obj->maxScoreOfLevel = JsonUtil::GetDoubleSafely (value, "maxScoreOfLevel");
        obj->levelName = JsonUtil::GetStringSafely (value, "levelName");
        return obj;
}


std::string ModelScoreLevel::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString(value);
}
