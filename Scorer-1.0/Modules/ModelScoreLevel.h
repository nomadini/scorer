#ifndef ModelScoreLevel_H
#define ModelScoreLevel_H

#include <memory>
#include <string>
#include "JsonTypeDefs.h"
class EntityToModuleStateStats;

class ModelScoreLevel;


class ModelScoreLevel {

private:

public:

double minScoreOfLevel;
double maxScoreOfLevel;
std::string levelName;
ModelScoreLevel();
virtual ~ModelScoreLevel();

static std::shared_ptr<ModelScoreLevel> fromJson(std::string jsonString);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
static std::shared_ptr<ModelScoreLevel> fromJsonValue(RapidJsonValueType value);

};
#endif
