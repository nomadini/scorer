
#include "ModelScoreLevelContainer.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"

#include <boost/foreach.hpp>

ModelScoreLevelContainer::ModelScoreLevelContainer() {
        modelScoreLevels = std::make_shared<std::vector<std::shared_ptr<ModelScoreLevel> > >();

}


ModelScoreLevelContainer::~ModelScoreLevelContainer() {
}

std::shared_ptr<ModelScoreLevelContainer> ModelScoreLevelContainer::fromJson(std::string jsonString) {
        auto document = parseJsonSafely(jsonString);
        return ModelScoreLevelContainer::fromJsonValue(*document);
}

void ModelScoreLevelContainer::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonArrayUtil::addMemberToValue_FromPair(doc, "modelScoreLevels", *modelScoreLevels, value);
}

std::shared_ptr<ModelScoreLevelContainer> ModelScoreLevelContainer::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<ModelScoreLevelContainer> obj = std::make_shared<ModelScoreLevelContainer>();

        JsonArrayUtil::getArrayFromValueMemeber(value,  "modelScoreLevels", *obj->modelScoreLevels);
        return obj;
}


std::string ModelScoreLevelContainer::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString(value);
}
