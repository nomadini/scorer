#ifndef ModelScoreLevelContainer_H
#define ModelScoreLevelContainer_H

#include <memory>
#include <string>
#include <vector>
#include "ModelScoreLevel.h"
#include "ConcurrentHashMap.h"

#include "JsonTypeDefs.h"

class EntityToModuleStateStats;

class ModelScoreLevelContainer;


class ModelScoreLevelContainer {

private:

public:

std::shared_ptr<std::vector<std::shared_ptr<ModelScoreLevel> > > modelScoreLevels;

ModelScoreLevelContainer();
virtual ~ModelScoreLevelContainer();

static std::shared_ptr<ModelScoreLevelContainer> fromJson(std::string jsonString);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
static std::shared_ptr<ModelScoreLevelContainer> fromJsonValue(RapidJsonValueType value);

};

#endif
