
#include "GUtil.h"

#include "Status.h"
#include "ScorerModule.h"
#include "StringUtil.h"

#include "DeviceSegmentHistoryCassandraService.h"
#include "CrossWalkDeviceSegmentPersistenceModule.h"
#include "DateTimeUtil.h"
#include "DeviceSegmentPair.h"
#include "CrossWalkReaderModule.h"
#include "EntityToModuleStateStats.h"
#include "DeviceFeatureHistory.h"
#include "Device.h"
#include "CrossWalkReaderModule.h"

CrossWalkDeviceSegmentPersistenceModule::CrossWalkDeviceSegmentPersistenceModule
								(DeviceSegmentHistoryCassandraService* deviceSegmentHistoryCassandraService) {
								this->deviceSegmentHistoryCassandraService = deviceSegmentHistoryCassandraService;
}

std::string CrossWalkDeviceSegmentPersistenceModule::getName() {
								return "CrossWalkDeviceSegmentPersistenceModule";
}


void CrossWalkDeviceSegmentPersistenceModule::process(std::shared_ptr<ScorerContext> context) {
								//read all devices cross walked to this device
								std::vector<std::shared_ptr<Device> > allCrosswalkedDevices =
																crossWalkReaderModule->readNearByDevices(context->deviceFeatureHistory->device);
								persistTheCrosswalkedDeviceSegments(context);
}

void CrossWalkDeviceSegmentPersistenceModule::persistTheCrosswalkedDeviceSegments(
								std::shared_ptr<ScorerContext> context) {
								for(std::shared_ptr<Segment> segment : *context->allSegmentsCreatedForDevice) {
																segment->setType(Segment::CrossWalkedSegmentType);

																auto deviceSegmentHistory = std::make_shared<DeviceSegmentHistory>(
																								context->deviceFeatureHistory->device
																								);

																auto deviceSegmentPair = std::make_shared<DeviceSegmentPair>(
																								context->deviceFeatureHistory->device,
																								segment
																								);

																deviceSegmentHistory->deviceSegmentAssociations->push_back(deviceSegmentPair);
																deviceSegmentHistoryCassandraService->pushToWriteBatchQueue(deviceSegmentHistory);
																entityToModuleStateStats->addStateModuleForEntity(
																								"ADDING_CROSSWALKED_DEVICE_SEGMENT",
																								"CrossWalkDeviceSegmentPersistenceModule",
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::important);

								}

}
void CrossWalkDeviceSegmentPersistenceModule::consumeMessage() {

}

CrossWalkDeviceSegmentPersistenceModule::~CrossWalkDeviceSegmentPersistenceModule() {

}
