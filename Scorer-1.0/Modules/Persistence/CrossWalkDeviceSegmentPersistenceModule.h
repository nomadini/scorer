/*
 * CrossWalkDeviceSegmentPersistenceModule.h
 *
 *  Created on: Oct 29, 2015
 *      Author: mtaabodi
 */
#ifndef CrossWalkDeviceSegmentPersistenceModule_H
#define CrossWalkDeviceSegmentPersistenceModule_H



#include "Status.h"
#include "ScorerModule.h"

#include <memory>
#include <string>
class DeviceSegmentHistoryCassandraService;

class CrossWalkReaderModule;
class CrossWalkDeviceSegmentPersistenceModule : public ScorerModule {

public:
DeviceSegmentHistoryCassandraService* deviceSegmentHistoryCassandraService;

CrossWalkReaderModule* crossWalkReaderModule;

CrossWalkDeviceSegmentPersistenceModule(
        DeviceSegmentHistoryCassandraService* deviceSegmentHistoryCassandraService);
std::string getName();

virtual void process(std::shared_ptr<ScorerContext> context);

void persistTheCrosswalkedDeviceSegments(std::shared_ptr<ScorerContext> context);
void consumeMessage();

virtual ~CrossWalkDeviceSegmentPersistenceModule();

};



#endif
