
#include "GUtil.h"

#include "Status.h"
#include "ScorerModule.h"
#include "StringUtil.h"

#include "DeviceSegmentHistoryCassandraService.h"
#include "DeviceSegmentPersistenceModule.h"
#include "DateTimeUtil.h"
#include "DeviceSegmentPair.h"
#include "EntityToModuleStateStats.h"
#include "DeviceFeatureHistory.h"

DeviceSegmentPersistenceModule::DeviceSegmentPersistenceModule
								(DeviceSegmentHistoryCassandraService* deviceSegmentHistoryCassandraService) {
								this->deviceSegmentHistoryCassandraService = deviceSegmentHistoryCassandraService;
}

std::string DeviceSegmentPersistenceModule::getName() {
								return "DeviceSegmentPersistenceModule";
}


void DeviceSegmentPersistenceModule::process(std::shared_ptr<ScorerContext> context) {


								persistTheMainDeviceSegments(context);

}


void DeviceSegmentPersistenceModule::persistTheMainDeviceSegments(std::shared_ptr<ScorerContext> context) {
								for(std::shared_ptr<Segment> segment : *context->allSegmentsCreatedForDevice) {
																auto deviceSegmentHistory = std::make_shared<DeviceSegmentHistory>(
																								context->deviceFeatureHistory->device
																								);

																auto deviceSegmentPair = std::make_shared<DeviceSegmentPair>(
																								context->deviceFeatureHistory->device,
																								segment
																								);

																deviceSegmentHistory->deviceSegmentAssociations->push_back(deviceSegmentPair);
																deviceSegmentHistoryCassandraService->pushToWriteBatchQueue(deviceSegmentHistory);
																entityToModuleStateStats->addStateModuleForEntity(
																								"ADDING_DEVICE_SEGMENT",
																								"DeviceSegmentPersistenceModule",
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::important);

																//we want to know what segments are being created for these devices
																//this a very importnat metric for scorer
																entityToModuleStateStats->addStateModuleForEntity(
																								"SEGMENT_ADDED_" + segment->getUniqueName(),
																								"DeviceSegmentPersistenceModule",
																								EntityToModuleStateStats::all);

								}

}
void DeviceSegmentPersistenceModule::consumeMessage() {

}

DeviceSegmentPersistenceModule::~DeviceSegmentPersistenceModule() {

}
