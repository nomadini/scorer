/*
 * DeviceSegmentPersistenceModule.h
 *
 *  Created on: Oct 29, 2015
 *      Author: mtaabodi
 */
#ifndef DeviceSegmentPersistenceModule_H
#define DeviceSegmentPersistenceModule_H



#include "Status.h"
#include "ScorerModule.h"

#include <memory>
#include <string>
class DeviceSegmentHistoryCassandraService;

class DeviceSegmentPersistenceModule : public ScorerModule {

public:
DeviceSegmentHistoryCassandraService* deviceSegmentHistoryCassandraService;

DeviceSegmentPersistenceModule(
        DeviceSegmentHistoryCassandraService* deviceSegmentHistoryCassandraService);
std::string getName();

virtual void process(std::shared_ptr<ScorerContext> context);

void persistTheMainDeviceSegments(std::shared_ptr<ScorerContext> context);
void consumeMessage();

virtual ~DeviceSegmentPersistenceModule();

};



#endif
