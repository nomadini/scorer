
#include "GUtil.h"

#include "Status.h"
#include "ScorerModule.h"
#include "StringUtil.h"

#include "SegmentToDeviceCassandraService.h"
#include "SegmentToDevicePersistenceModule.h"
#include "Segment.h"
#include "DateTimeUtil.h"
#include "DeviceFeatureHistory.h"
#include "EntityToModuleStateStats.h"
SegmentToDevicePersistenceModule::SegmentToDevicePersistenceModule(
								SegmentToDeviceCassandraService* segmentToDeviceCassandraService) {
								this->segmentToDeviceCassandraService = segmentToDeviceCassandraService;
}

std::string SegmentToDevicePersistenceModule::getName() {
								return "SegmentToDevicePersistenceModule";
}

void SegmentToDevicePersistenceModule::process(std::shared_ptr<ScorerContext> context) {

								for(auto segment : *context->allSegmentsCreatedForDevice) {
																std::shared_ptr<SegmentDevices> segmentDevices = std::make_shared<SegmentDevices>();
																segmentDevices->segment = segment;
																segmentDevices->devices.push_back(
																								context->deviceFeatureHistory->device);

																segmentToDeviceCassandraService->pushToWriteBatchQueue(segmentDevices);
																entityToModuleStateStats->addStateModuleForEntity(
																								"ADDING_SEGMENT_DEVICE",
																								"SegmentToDevicePersistenceModule",
																								"ALL");
								}



}

void SegmentToDevicePersistenceModule::consumeMessage() {

}

SegmentToDevicePersistenceModule::~SegmentToDevicePersistenceModule() {

}
