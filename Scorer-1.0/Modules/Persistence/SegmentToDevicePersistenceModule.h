/*
 * SegmentToDevicePersistenceModule.h
 *
 *  Created on: Oct 29, 2015
 *      Author: mtaabodi
 */
#ifndef SegmentToDevicePersistenceModule_H
#define SegmentToDevicePersistenceModule_H



#include "Status.h"
#include "ScorerModule.h"
class SegmentToDeviceCassandraService;
#include <memory>
#include <string>

class SegmentToDevicePersistenceModule : public ScorerModule {

public:
SegmentToDeviceCassandraService* segmentToDeviceCassandraService;
SegmentToDevicePersistenceModule(
        SegmentToDeviceCassandraService* segmentToDeviceCassandraService);
std::string getName();

virtual void process(std::shared_ptr<ScorerContext> context);

void consumeMessage();

virtual ~SegmentToDevicePersistenceModule();



};



#endif
