#include "GUtil.h"
#include "Segment.h"
#include "ScoreToSegmentConverter.h"
#include "DateTimeUtil.h"
#include "SegmentNameCreator.h"
#include "ModelScoreLevelContainer.h"
#include "SegmentCacheService.h"
#include "MySqlSegmentService.h"
#include "ConfigService.h"

ScoreToSegmentConverter::ScoreToSegmentConverter() {

}

std::shared_ptr<Segment> ScoreToSegmentConverter::toSegment(
								double score,
								std::shared_ptr<ModelRequest> model,
								std::shared_ptr<ModelScoreLevelContainer> modelScoreLevel) {
								std::shared_ptr<Segment> segment = std::make_shared<Segment>(DateTimeUtil::getNowInMilliSecond());

								// std::string segmentLevel = getSegmentLevel(score);
								std::string segmentLevel = getSegmentLevelV2(score, model, modelScoreLevel);
								segment->setUniqueName(
																SegmentNameCreator::createName(
																								model->segmentSeedName,
																								segmentLevel,
																								"MD"));
								segment->advertiserId = model->advertiserId;
								segment->setType(Segment::ModelBasedSegmentName);
								segment->modelId = model->id;

								auto segmentInCachePair =
																segmentCacheService->
																allNameToSegmentsMap->find (segment->getUniqueName());
								if (segmentInCachePair != segmentCacheService->allNameToSegmentsMap->end()) {
																return segmentInCachePair->second;
								} else {
																return insertSegmentBlocking(segment);
								}

								return segment;
}

std::shared_ptr<Segment> ScoreToSegmentConverter::insertSegmentBlocking(
								std::shared_ptr<Segment> segment) {

								// get upgradable access
								boost::upgrade_lock<boost::shared_mutex> lock(_access);

								// get exclusive access
								boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
								// now we have exclusive access


								// we check the cache again after acquiring the lock
								//maybe some other thread, already put insert the segment and populated the cache
								auto segmentInCachePair =
																segmentCacheService->
																allNameToSegmentsMap->find (segment->getUniqueName());
								if (segmentInCachePair != segmentCacheService->allNameToSegmentsMap->end()) {
																return segmentInCachePair->second;
								}

								mySqlSegmentService->insertIfNotExisting(segment);
								auto segmentPersistedInDb = mySqlSegmentService->readSegment(
																segment->getUniqueName(),
																segment->advertiserId,
																segment->modelId
																);
								segmentCacheService->allNameToSegmentsMap->insert(
																std::make_pair(segment->getUniqueName(), segment));

								assertAndThrow(segmentPersistedInDb->id > 0);
								return segmentPersistedInDb;

}
std::string ScoreToSegmentConverter::getSegmentLevelV2(
								double score,
								std::shared_ptr<ModelRequest> model,
								std::shared_ptr<ModelScoreLevelContainer> modelScoreLevelContainer) {


								std::shared_ptr<std::vector<std::shared_ptr<ModelScoreLevel> > > modelScoreLevels =
																modelScoreLevelContainer->modelScoreLevels;
								for (auto modelScoreLevel : *modelScoreLevels) {
																if (modelScoreLevel->minScoreOfLevel >= score  &&
																				score <= modelScoreLevel->maxScoreOfLevel) {
																								return modelScoreLevel->levelName;
																}

								}

								throwEx("couldn't find a level for score " + _toStr(score) + " in model score list");
}

std::string ScoreToSegmentConverter::getSegmentLevel(double score) {
								std::string segmentLevel;

								if (score >= 12.00) {
																segmentLevel = StringUtil::toStr("_A");
								} else if (score >= 11.00 && score < 12.00) {
																segmentLevel = StringUtil::toStr("_B");
								} else if (score >= 9.00 && score < 10.00) {
																segmentLevel = StringUtil::toStr("_C");

								} else if (score >= 8.00 && score < 9.00) {
																segmentLevel = StringUtil::toStr("_D");

								} else if (score >= 7.00 && score < 8.00) {
																segmentLevel = StringUtil::toStr("_E");

								} else if (score >= 6.00 && score < 7.00) {
																segmentLevel = StringUtil::toStr("_F");

								} else if (score > 5.00 && score < 6.00) {
																segmentLevel = StringUtil::toStr("_G");

								} else if (score > 4.00 && score < 5.00) {
																segmentLevel = StringUtil::toStr("_H");

								} else if (score > 3.00 && score < 4.00) {
																segmentLevel = StringUtil::toStr("_I");
								} else {
																segmentLevel = StringUtil::toStr("_Z");
								}
								MLOG(3)<<"segmentLevel : "<<segmentLevel;
								return segmentLevel;
}

ScoreToSegmentConverter::~ScoreToSegmentConverter() {

}

std::shared_ptr<ScoreToSegmentConverter> ScoreToSegmentConverter::getInstance(
								gicapods::ConfigService* configService) {

								std::shared_ptr<ScoreToSegmentConverter> ins =
																std::make_shared<ScoreToSegmentConverter>();
								ins->mySqlSegmentService =
																MySqlSegmentService::getInstance(configService).get();
								ins->segmentCacheService =
																SegmentCacheService::getInstance(configService).get();
								return ins;
}
