/*
 * ScoreToSegmentConverter.h
 *
 *  Created on: Aug 1, 2015
 *      Author: mtaabodi
 */

#ifndef ScoreToSegmentConverter_H_
#define ScoreToSegmentConverter_H_



#include "Segment.h"
#include "ModelRequest.h"
#include "ModelRequest.h"

#include <memory>
#include <string>
class ModelScoreLevelContainer;
class SegmentCacheService;
class MySqlSegmentService;
class ScoreToSegmentConverter;
#include <boost/thread.hpp>
namespace gicapods {
class ConfigService;
}
class ScoreToSegmentConverter {

public:

boost::shared_mutex _access;

SegmentCacheService* segmentCacheService;
MySqlSegmentService* mySqlSegmentService;

ScoreToSegmentConverter();

std::shared_ptr<Segment> toSegment(double score,
                                   std::shared_ptr<ModelRequest> model,
                                   std::shared_ptr<ModelScoreLevelContainer> modelScoreLevel);

std::string getSegmentLevel(double score);
std::string getSegmentLevelV2(
        double score,
        std::shared_ptr<ModelRequest> model,
        std::shared_ptr<ModelScoreLevelContainer> modelScoreLevel);

virtual ~ScoreToSegmentConverter();
std::shared_ptr<Segment> insertSegmentBlocking(std::shared_ptr<Segment> segment);
static std::shared_ptr<ScoreToSegmentConverter> getInstance(gicapods::ConfigService* configService);
};



#endif /* ScoreToSegmentConverter_H_ */
