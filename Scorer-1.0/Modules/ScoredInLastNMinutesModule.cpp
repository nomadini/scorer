
#include "ScoredInLastNMinutesModule.h"
#include "EntityToModuleStateStats.h"
#include <boost/foreach.hpp>
#include "LastTimeSeenSource.h"

ScoredInLastNMinutesModule::ScoredInLastNMinutesModule(int intervalOfScoringADeviceInSeconds) {
								this->intervalOfScoringADeviceInSeconds = intervalOfScoringADeviceInSeconds;
}

std::string ScoredInLastNMinutesModule::getName() {
								return "ScoredInLastNMinutesModule";
}

void ScoredInLastNMinutesModule::process(std::shared_ptr<ScorerContext> context) {


								if (directLastTimeSeenSource->hasSeenInLastXSeconds(
																				context->transAppMessage->device)) {
																context->status->value = STOP_PROCESSING;
																entityToModuleStateStats->addStateModuleForEntity (
																								"SCORED_RECENTLY_SKIPPING",
																								"ScoredInLastNMinutesModule",
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::important);
								} else {
																directLastTimeSeenSource->markAsSeenInLastXSeconds(
																								context->transAppMessage->device,
																								intervalOfScoringADeviceInSeconds);
								}
}

ScoredInLastNMinutesModule::~ScoredInLastNMinutesModule() {
}
