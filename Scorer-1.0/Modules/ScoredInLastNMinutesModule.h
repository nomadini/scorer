#ifndef ScoredInLastNMinutesModule_H
#define ScoredInLastNMinutesModule_H


#include "Status.h" //this is needed in every module
#include "ScorerContext.h"
#include <memory>
#include <string>
#include "ScorerModule.h"
class LastTimeSeenSource;
class EntityToModuleStateStats;
class ScoredInLastNMinutesModule;


class ScoredInLastNMinutesModule : public ScorerModule {

private:
int intervalOfScoringADeviceInSeconds;
public:
EntityToModuleStateStats* entityToModuleStateStats;
std::unique_ptr<LastTimeSeenSource> directLastTimeSeenSource;

ScoredInLastNMinutesModule(int intervalOfScoringADeviceInSeconds);

virtual void process(std::shared_ptr<ScorerContext> context);

virtual std::string getName();

virtual ~ScoredInLastNMinutesModule();
};



#endif
