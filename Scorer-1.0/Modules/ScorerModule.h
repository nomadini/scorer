#ifndef ScorerModule_H
#define ScorerModule_H


#include "Status.h" //this is needed in every module
#include "ScorerContext.h"
#include <memory>
#include <string>
class EntityToModuleStateStats;

class ScorerModule;


class ScorerModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
ScorerModule();

virtual void beforeProcessing(std::shared_ptr<ScorerContext> context);

virtual void process(std::shared_ptr<ScorerContext> context) = 0;

virtual void afterProcessing(std::shared_ptr<ScorerContext> context);

virtual std::string getName() = 0;

virtual ~ScorerModule();

};
#endif
