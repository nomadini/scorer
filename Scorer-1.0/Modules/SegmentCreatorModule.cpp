
#include "GUtil.h"
#include "StringUtil.h"
#include "Status.h"
#include "ScorerModule.h"
#include "ScoreToSegmentConverter.h"
#include "SegmentCreatorModule.h"
#include "ModelScoreLevelContainer.h"
#include "EntityToModuleStateStats.h"
#include "ScorerContext.h"
#include "ModelScore.h"

SegmentCreatorModule::SegmentCreatorModule() {

}

std::string SegmentCreatorModule::getName() {
								return "SegmentCreatorModule";
}

void SegmentCreatorModule::process(std::shared_ptr<ScorerContext> context) {

								for(auto&& modelPair : *context->allLoadedModels) {

																tbb::concurrent_hash_map<int, double>::accessor scoreAccessor;
																if(context->modelIdToScoreMap->find (scoreAccessor, modelPair.first)) {
																								double score = scoreAccessor->second;
																								auto model = modelPair.second;

																								auto modelScore = std::make_shared<ModelScore>();
																								modelScore->modelId = model->id;
																								modelScore->score = score;
																								modelScoreCassandraService->pushToWriteBatchQueue(modelScore);

																								auto modelScoreLevel = getModelScoreLevel(model->id);
																								if (modelScoreLevel == nullptr) {
																																entityToModuleStateStats->addStateModuleForEntity (
																																								"Aborted_NO_MODEL_SCORE_CLASS:" + _toStr(model->id),
																																								"SegmentCreatorModule",
																																								EntityToModuleStateStats::all);


																								} else {
																																auto segment = scoreToSegmentConverter->toSegment(score, model, modelScoreLevel);
																																context->allSegmentsCreatedForDevice->push_back(segment);
																								}

																} else {
																								throwEx("model with id "+ StringUtil::toStr(modelPair.first) + " not in map!");
																}


								}
}


std::shared_ptr<ModelScoreLevelContainer> SegmentCreatorModule::getModelScoreLevel(int modelId) {

								auto modelScoreLevel = modelIdToModelScoreLevelMap->getOptional(modelId);
								return modelScoreLevel;
}

SegmentCreatorModule::~SegmentCreatorModule() {

}
