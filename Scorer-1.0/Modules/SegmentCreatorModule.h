/*
 * SegmentCreatorModule.h
 *
 *  Created on: Oct 29, 2015
 *      Author: mtaabodi
 */
#ifndef SegmentCreatorModule_H
#define SegmentCreatorModule_H



#include "Status.h"
#include "ScorerModule.h"
#include "ScoreToSegmentConverter.h"
#include "ScorerContext.h"
#include "ModelScoreLevelContainer.h"
#include "ModelScore.h"
#include "ConcurrentHashMap.h"
#include "CassandraService.h"
#include <memory>
#include <string>

class SegmentCreatorModule : public ScorerModule {

public:
ScoreToSegmentConverter* scoreToSegmentConverter;
CassandraService<ModelScore>* modelScoreCassandraService;
std::shared_ptr<gicapods::ConcurrentHashMap<int, ModelScoreLevelContainer > > modelIdToModelScoreLevelMap;

SegmentCreatorModule();

std::string getName();

virtual void process(std::shared_ptr<ScorerContext> context);
virtual ~SegmentCreatorModule();

std::shared_ptr<ModelScoreLevelContainer> getModelScoreLevel(int modelId);
};



#endif
