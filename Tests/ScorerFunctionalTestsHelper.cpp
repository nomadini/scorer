
#include "ScorerFunctionalTestsHelper.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "SvmSgdScorer.h"
#include <string>
#include <memory>


std::shared_ptr<SvmSgdScorer> ScorerFunctionalTestsHelper::scorer;
int ScorerFunctionalTestsHelper::argc;
char** ScorerFunctionalTestsHelper::argv;

void ScorerFunctionalTestsHelper::init(int argc, char *argv[]) {
        ScorerFunctionalTestsHelper::argc = argc;
        ScorerFunctionalTestsHelper::argv = argv;
}
