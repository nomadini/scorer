//
// Created by Mahmoud Taabodi on 11/29/15.
//

#ifndef ScorerFunctionalTestsHelper_H
#define ScorerFunctionalTestsHelper_H




class TargetGroupCacheService;
class CampaignCacheService;
#include "MySqlCampaignService.h"
#include "MySqlGeoSegmentListService.h"
#include "MySqlSegmentService.h"
#include "MySqlAdvertiserModelMappingService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlModelService.h"
#include "MySqlOfferService.h"
#include "StringUtil.h"
#include "SvmSgdScorer.h"
class ScorerFunctionalTestsHelper {

public:
static std::shared_ptr<SvmSgdScorer> scorer;
static int argc;
static char** argv;
static void init(int argc, char *argv[]);
};
#endif //ScorerFunctionalTestsHelper_H
