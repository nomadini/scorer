
#include "ScorerTestFixture.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "Segment.h"
#include "DeviceFeatureHistory.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "TestUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "ScorerFunctionalTestsHelper.h"
#include "DeviceFeatureHistoryTestHelper.h"
#include "MySqlAdvertiserModelMappingService.h"
#include "ConcurrentQueueFolly.h"
#include "DeviceSegmentHistoryCassandraService.h"
#include "SegmentToDeviceCassandraService.h"
#include "MySqlModelService.h"
#include <boost/foreach.hpp>
#include "ModelRequestTestHelper.h"
#include "TargetGroupTestHelper.h"
#include "CampaignTestHelper.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCampaignService.h"
#include "TargetGroupTestUtil.h"
#include "MySqlSegmentService.h"
#include "SegmentTestHelper.h"
#include "TargetGroup.h"
#include "Campaign.h"
#include "CollectionUtil.h"

#include "DateTimeUtil.h"
#include "BeanFactory.h"
#include <thread>
#include "CassandraDriverInterface.h"
#include "SvmSgdScorer.h"

ScorerTestFixture::ScorerTestFixture() {

}

ScorerTestFixture::~ScorerTestFixture() {

}

void ScorerTestFixture::theDeviceSegmentsArePersisted() {
//std::string deviceId, std::string deviceType
        // this->actualDeviceSegmentInDb = beanFactory->deviceSegmentHistoryCassandraService->readOptional(this->deviceId, this->deviceType);

        // DeviceSegment::areEqual(this->actualDeviceSegmentInDb, this->expectedDeviceSegments);
}

void ScorerTestFixture::thenSegmentDevicesArePersisted() {
        this->actualSegmentDevicesInDb =  beanFactory->segmentToDeviceCassandraService->read(expectedSegments.at(0)->getUniqueName());
        SegmentDevices::areEqual(this->actualSegmentDevicesInDb, this->expectedSegmentDevices);
}

std::vector<std::string> ScorerTestFixture::givenDeviceHistoriesInDbForLast5Min() {

        std::vector<std::string> seedWebsites;
        std::vector<std::string> importantWebsites;
        for(int i=0; i<100; i++) {
                // seedWebsites.push_back(StringUtil::toStr("cnn.com").append(StringUtil::random_string(3)));
        }

        for(int i=0; i<5; i++) {
                // importantWebsites.push_back(StringUtil::toStr("important.com").append(StringUtil::random_string(3)));
        }
        std::vector<std::string> allWebsites = CollectionUtil::combineTwoLists<std::string>(seedWebsites, importantWebsites);

        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                std::make_shared<DeviceFeatureHistory>(
                        std::make_shared<Device>(Device::createStandardDeviceId(), "DESKTOP"));
        this->deviceId = deviceFeatureHistory->device->getDeviceId();
        this->deviceType = deviceFeatureHistory->device->getDeviceType();

        for(std::string seedWebsite :  allWebsites) {
                // deviceFeatureHistory->timeFeatureMap.insert(std::pair<TimeType, std::shared_ptr<Feature> >(
                //                                                     DateTimeUtil::getNowInMicroSecond(),
                //                                                     seedWebsite));

        }

        return importantWebsites;

}
void ScorerTestFixture::givenModelExistForAdvertiser(std::vector<std::string> importantWebsites) {

        std::unordered_map<std::string, double> featureScoreMap;
        for(std::string website :  importantWebsites) {
                featureScoreMap.insert(std::pair<std::string, double>(website, 1.003));
        }

        std::shared_ptr<ModelRequest> model = ModelRequestTestHelper::createBuiltPixelModelWithFeatures(featureScoreMap);

        this->targetGroup = TargetGroupTestHelper::createSampleTargetGroup();
        auto campaign = CampaignTestHelper::createSampleCampaign();
        this->targetGroup->setCampaignId(campaign->getId ());
        campaign->setAdvertiserId(model->advertiserId);

        beanFactory->mySqlModelService->insert(model);


        beanFactory->mySqlCampaignService->insert(campaign);
        beanFactory->mySqlTargetGroupService->insert(this->targetGroup);

        auto targetGroup = beanFactory->mySqlTargetGroupService->readTargetGroupById(this->targetGroup->getId());
        TargetGroupTestUtil::areEqual(targetGroup, this->targetGroup);

        std::shared_ptr<AdvertiserModelMapping> advertiserModelMap(new AdvertiserModelMapping());
        advertiserModelMap->modelId = model->id;
        advertiserModelMap->advertiserId = model->advertiserId;
        advertiserModelMap->status = "active";
        beanFactory->mySqlAdvertiserModelMappingService->insert(advertiserModelMap);
        this->model = model;

        std::shared_ptr<Segment> expectedSegment = std::make_shared<Segment>(
                DateTimeUtil::getNowInMilliSecond()
                );
        expectedSegment->advertiserId = model->advertiserId;
        expectedSegment->modelId = model->id;
        expectedSegment->setUniqueName("hassan-pixelModel__G");
        this->expectedSegments.push_back(expectedSegment);


        // this->expectedDeviceSegments
        // expectedDeviceSegments->segmentIds.push_back(expectedSegment->getUniqueName());
        // expectedDeviceSegments->dateCreated =  DateTimeUtil::getNowInYYYMMDDFormat();
        // expectedDeviceSegments->deviceId = this->deviceId;
        // expectedDeviceSegments->deviceType = this->deviceType;

        std::vector<std::shared_ptr<Device> > devices;
        devices.push_back(std::make_shared<Device>(this->deviceId,this->deviceType));
        this->expectedSegmentDevices = std::make_shared<SegmentDevices>();

        expectedSegmentDevices->devices = devices;
        expectedSegmentDevices->segment = expectedSegment;

        MLOG(3)<<"size of expectedSegments : "<<expectedSegments.size();


        //Todo :
// auto campaign = beanFactory->mySqlCampaignService->readCampaignById(campaign->id);
// CampaignTestUtil::areEqual(campaign, this->campaign);
//
// auto model = beanFactory->mySqlModelService->readModelById(model);
// ModelTestUtil::areEqual(model, this->model);

}


void ScorerTestFixture::whenScoringRuns() {
        std::thread scorerTestRunner(ScorerTestFixture::runScoring);
        scorerTestRunner.detach();

        MLOG(3)<<"waiting for scorer to finish processing..";

        MLOG(3)<<"waiting for scorer to finish writing the segment..";
        std::shared_ptr<Segment> segment;
        // ConcurrentQueueFolly::getSegmentInsertedQueue()->blockingRead(segment);
}

void ScorerTestFixture::runScoring() {

}

void ScorerTestFixture::thenSegmentsAreCreatedForDevicesAsExpected() {

}
void ScorerTestFixture::thenSegmentsArePersistedInDb(){

        std::vector<std::shared_ptr<Segment> > actualSegments =
                beanFactory->mySqlSegmentService->readAllSegmentForAdvertiserAndModel(this->model->advertiserId, this->model->id);

        std::unordered_map<std::string, std::shared_ptr<Segment> > actualSegmentsMap;
        for(std::shared_ptr<Segment> seg :  actualSegments) {
                actualSegmentsMap.insert(std::make_pair(seg->getUniqueName(), seg));
        }

        for(std::shared_ptr<Segment> segment :  actualSegments) {
                MLOG(3)<<"segments created : "<<segment->toString();
        }

        for(std::shared_ptr<Segment> segment :  this->expectedSegments) {
                MLOG(3)<<"segments expected : "<<segment->toString();
        }
        EXPECT_GT(actualSegments.size(), 0);
        EXPECT_EQ(this->expectedSegments.size(), actualSegments.size());

        for(std::shared_ptr<Segment> expectedSegment :  this->expectedSegments) {
                auto segPairPtr = actualSegmentsMap.find(expectedSegment->getUniqueName());
                if (segPairPtr!=actualSegmentsMap.end()) {
                        MLOG(3)<<"segment created is : "<<segPairPtr->second->toString();
                        SegmentTestHelper::areEqual(expectedSegment, segPairPtr->second);
                } else {
                        MLOG(3)<<"segment was not found in the map : "<<expectedSegment->getUniqueName();
                        EXPECT_FALSE(true);
                }
        }
}
