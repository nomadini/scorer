#ifndef ScorerTestFixture_H
#define ScorerTestFixture_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include <gtest/gtest.h>
#include "TestsCommon.h"
class StringUtil;
#include "GeoSegment.h"
#include "Segment.h"
#include "ModelRequest.h"
#include "TargetGroupTypeDefs.h"
#include "BeanFactory.h"
class TargetGroup;
#include "SvmSgdScorer.h"

#include "SegmentDevices.h"
class ScorerTestFixture;

class ScorerTestFixture {

public:

std::vector<std::shared_ptr<GeoSegment> > segmentsCreated;
std::shared_ptr<ModelRequest> model;
std::shared_ptr<TargetGroup> targetGroup;
std::string deviceId;
std::string deviceType;
std::vector<std::shared_ptr<DeviceSegmentHistory> > actualDeviceSegmentInDb;
std::vector<std::shared_ptr<DeviceSegmentHistory> > expectedDeviceSegments;

std::vector<std::shared_ptr<Segment> > expectedSegments;
std::shared_ptr<BeanFactory> beanFactory;
std::shared_ptr<SegmentDevices> actualSegmentDevicesInDb;
std::shared_ptr<SegmentDevices> expectedSegmentDevices;


static std::shared_ptr<SvmSgdScorer> getScorer();

ScorerTestFixture();
virtual ~ScorerTestFixture();
void setUp();
void tearDown();
static void runScoring();
std::vector<std::string> givenDeviceHistoriesInDbForLast5Min();
void givenModelExistForAdvertiser(std::vector<std::string> importantWebsites);
void whenScoringRuns();
void thenSegmentsAreCreatedForDevicesAsExpected();
void thenSegmentsArePersistedInDb();
void theDeviceSegmentsArePersisted();

void thenSegmentDevicesArePersisted();

};

#endif
