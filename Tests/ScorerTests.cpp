/*
 * Scorer.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#include "GUtil.h"
#include "CassandraDriverInterface.h"
#include "ScorerFunctionalTestsHelper.h"
#include "ConfigService.h"

#include "GUtil.h"
#include "TempUtil.h"
#include "StringUtil.h"
#include "TestsCommon.h"
#include "ScorerTestFixture.h"
#include "BeanFactory.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include <thread>
int runGoogleTests(int argc, char** argv) {
        // The following line must be executed to initialize Google Mock
        // (and Google Test) before running the tests.
        ::testing::InitGoogleMock(&argc, argv);
        ::testing::FLAGS_gmock_verbose ="info";

        return RUN_ALL_TESTS();
}


void runScorer(std::shared_ptr<BeanFactory> beanFactory){
        // auto scorer = std::make_shared<SvmSgdScorer>(
        //         beanFactory->mySqlSegmentService.get(),
        //         beanFactory->mySqlAdvertiserModelMappingService.get(),
        //         beanFactory->modelCacheService.get(),
        //         beanFactory->deviceFeatureHistoryCassandraService.get());
//    scorer->run(ScorerFunctionalTestsHelper::argc, ScorerFunctionalTestsHelper::argv);
}

void givenScorerRunning(std::shared_ptr<BeanFactory> beanFactory) {

        std::thread everyMinuteThread(runScorer, beanFactory);
        gicapods::Util::sleepViaBoost(_L_, 2);//wait for Scorer to start up!!
        everyMinuteThread.detach();
}

int main(int argc, char** argv) {
        SignalHandler::installHandlers();

        MLOG(3)<<" argc :  "<<argc<<" , argv : "<< *argv;

        ScorerFunctionalTestsHelper::init(argc,argv);

        std::string appName = "ScorerTests";
        TempUtil::configureLogging (appName, argv);
        std::string propertyFileName = "ScorerTests.properties";
        auto beanFactory = std::make_shared<BeanFactory>("testVersion1.0");
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->appName = appName;
        beanFactory->initializeModules();

        givenScorerRunning(beanFactory);

        runGoogleTests(argc, argv);

}
