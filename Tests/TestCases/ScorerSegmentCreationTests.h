//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef ScorerSegmentCreationTests_H
#define ScorerSegmentCreationTests_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
class StringUtil;
#include <string>
#include <memory>
class ScorerSegmentCreationTests;

class ScorerSegmentCreationTests : public ::testing::Test {

public:

//    BidderTestServicePtr testService;
//    BidderTestFixturePtr testFixture;
ScorerSegmentCreationTests();
void SetUp();

void TearDown();
virtual ~ScorerSegmentCreationTests();
};
#endif //ScorerSegmentCreationTests_H
