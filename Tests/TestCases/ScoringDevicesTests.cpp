#include "GUtil.h"


#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "TestsCommon.h"
#include "DeviceHistory.h"
#include "Offer.h"
#include "Segment.h"
#include "ScoringDevicesTests.h"
#include "MySqlModelService.h"
#include "PixelDeviceHistory.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include "ScorerTestFixture.h"
#include "MySqlTargetGroupService.h"
#include "MySqlOfferService.h"
#include "MySqlModelService.h"
#include "MySqlCampaignService.h"
#include "MySqlAdvertiserModelMappingService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "MySqlSegmentService.h"
#include "DeviceSegmentHistoryCassandraService.h"
#include "SegmentToDeviceCassandraService.h"
#include "BeanFactory.h"
#include "ScorerFunctionalTestsHelper.h"

ScoringDevicesTests::ScoringDevicesTests() {

        std::shared_ptr<ScorerTestFixture> testFixture = std::make_shared<ScorerTestFixture> ();
        testFixture = testFixture;
        // initialization code here
}

void ScoringDevicesTests::SetUp() {
        // code here will execute just before the test ensues

        beanFactory->mySqlCampaignService->deleteAll ();
        beanFactory->mySqlOfferService->deleteAllOffersAndAssociatedPixelsAndModels ();
        beanFactory->mySqlModelService->deleteAll ();
        beanFactory->mySqlTargetGroupService->deleteAll();
        beanFactory->mySqlAdvertiserModelMappingService->deleteAll();
        beanFactory->mySqlSegmentService->deleteAll();

        beanFactory->deviceSegmentHistoryCassandraService->deleteAll();
        beanFactory->segmentToDeviceCassandraService->deleteAll();
        beanFactory->deviceFeatureHistoryCassandraService->deleteAll();
        beanFactory->featureDeviceHistoryCassandraService->deleteAll();
}

void ScoringDevicesTests::TearDown() {
        // code here will be called just after the test completes
        // ok to through exceptions from here if need be

//       beanFactory->mySqlOfferService()->deleteAllOffersAndAssociatedPixelsAndModels;
//      beanFactory->mySqlModelService()->deleteAllModels;
//
//        MLOG(3)<<"going to sleep to  let the threads break out of their loops....";
//        gicapods::Util::sleepViaBoost(_L_,5);


}

ScoringDevicesTests::~ScoringDevicesTests() {
        // cleanup any pending stuff, but no exceptions allowed
}


/*
   in this test we send a bid request suited for a target group with a pacing plan
   then we see that after adserver shows the creative for target group
   we see that the campaign and target group real time infos have been
   updated in Pacer and in cassandra
 */
TEST_F(ScoringDevicesTests, fetchLastFiveMinuteDevicesAndScore) {
        MLOG(3) << "starting fetchLastFiveMinuteDevicesAndScore test";

        std::vector<std::string> importantWebsites = testFixture->givenDeviceHistoriesInDbForLast5Min ();
        testFixture->givenModelExistForAdvertiser (importantWebsites);
        testFixture->whenScoringRuns ();

        testFixture->thenSegmentsAreCreatedForDevicesAsExpected ();
        testFixture->thenSegmentsArePersistedInDb ();

        testFixture->theDeviceSegmentsArePersisted ();
        testFixture->thenSegmentDevicesArePersisted ();
//     ScorerContext::getScorerInterruptedFlag()->set(true);
        MLOG(3) << "fetchLastFiveMinuteDevicesAndScore....";

}




//TEST_F(ScoringDevicesTests, loadTestScoring) {
//
//    MLOG(3)<<"starting sendPixelModelRequestAndVerifyModelWasBuilt test";
//
////    std::shared_ptr<Offer> offer = ScoringDevicesTests::givenDataOfferDataInMySql();
////    auto pixelModelRequest = ScoringDevicesTests::givenAPixelModelRequest(offer);
////    auto eventListener = ScoringDevicesTests::listenForEvents(std::dynamic_pointer_cast<ModelRequest>(pixelModelRequest));
////    ScoringDevicesTests::givenFeatureAndDeviceHistoryInCassandra(offer);
////
////    std::vector<std::shared_ptr<ModelRequest>> models = ScoringDevicesTests::whenRequestHitsTheModelerService(pixelModelRequest->toJson());
////
////    std::shared_ptr<ModelRequest> actual  = models.at(0);
////    std::shared_ptr<ModelRequest> expected = std::dynamic_pointer_cast<ModelRequest>(pixelModelRequest);
////    ScoringDevicesTests::thenModelCreatedIsAsExpected(actual , expected , StringUtil::toStr("pixelModel"));
////
////    ScoringDevicesTests::thenModelExistsInDb(expected);
////    ScoringDevicesTests::thenTestWaitsUntilModelingIsDone();
//
//    MLOG(3)<<"end of test!";
//
//}
