//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef ScoringDevicesTests_H
#define ScoringDevicesTests_H




class CassandraDriverInterface;
#include "TestsCommon.h"
class DeviceHistory;
class BeanFactory;
#include <string>
#include <memory>
#include "ScorerTestFixture.h"
class ScoringDevicesTests : public ::testing::Test {


public:

std::shared_ptr<ScorerTestFixture> testFixture;
std::shared_ptr<BeanFactory> beanFactory;

ScoringDevicesTests();

void SetUp();
void TearDown();
virtual ~ScoringDevicesTests( );

};
#endif //ScoringDevicesTests_H
