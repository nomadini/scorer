//
// Created by Mahmoud Taabodi on 7/10/16.
//


#include "GUtil.h"
#include "SvmSgdScorer.h"
#include "ModelRequest.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "KafkaConsumer.h"
#include "KafkaProducer.h"
#include "ScoringMessageProcessor.h"
#include "DeviceFeatureHistoryTestHelper.h"
#include "ConfigService.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "TempUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "DeviceFeatureHistory.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "GalaxieSetup.h"
#include <boost/thread/thread.hpp>
#include "GalaxieSetup.h"
#include "BeanFactory.h"
#include "RandomUtil.h"
#include "AsyncThreadPoolService.h"

#include "BeanFactory.h"

std::shared_ptr<KafkaProducer> kafkaProducer;

std::shared_ptr<KafkaConsumer> kafkaConsumer;
/**
 * this will create device history for offer ids who are action takers
 */
void insertActionTakersDeviceFeatureHistoryNewStyle(std::string seedWebsite) {
        std::vector<std::string> allActionTakersBrowserIds;
        /**
         * creating a number of action takers browser ids
         */
        for (int i = 0; i < 10; i++) {
                std::string deviceId = StringUtil::toStr(RandomUtil::sudoRandomNumber(100000000000));


                std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                        DeviceFeatureHistoryTestHelper::createNewDeviceFeatureHistoryFromActionTakersPool(
                                seedWebsite);
                deviceFeatureHistory->device = std::make_shared<Device>(deviceId, "DESKTOP");

                allActionTakersBrowserIds.push_back(deviceFeatureHistory->device->getDeviceId());
                MLOG(3)<<"putting this deviceId  "<<deviceFeatureHistory->device->getDeviceId()<<" into cassandra";
                //TODO :make this a UNIT test later
                // beanFactory->deviceFeatureHistoryCassandraService->upsertDeviceFeatureHistory(bh);
        }

        for (auto deviceId : allActionTakersBrowserIds) {

                kafkaProducer->produceMessage(deviceId);
                MLOG(3)<<"put this message in kafka "<<deviceId;

        }
}
void consumeBrowsersInKafka() {
        kafkaConsumer->consume();
}
void populateBrowsersInCassandraAndKafka() {

        insertActionTakersDeviceFeatureHistoryNewStyle("abc.com");
}

void startProducerThreads(boost::thread_group& threads) {
        LOG(INFO)<<"starting producer threads";
        int numberOfBrowserIds = 1;
        for (int i = 0; i != numberOfBrowserIds; ++i) {
                threads.create_thread(populateBrowsersInCassandraAndKafka);
        }

}

void startConsumerThreads(boost::thread_group& threads) {
        LOG(INFO)<<"starting consumer threads";
        int numberOfBrowserIds = 1;
        for (int i = 0; i != numberOfBrowserIds; ++i) {
                threads.create_thread(consumeBrowsersInKafka);
        }
}

void testScoring() {
        boost::thread_group threads;
        startProducerThreads(threads);
        LOG(INFO)<<"sleeping....";
        gicapods::Util::sleepViaBoost(_L_,2);
        startConsumerThreads(threads);
        threads.join_all();

}

void readProperties() {

}


int mainTest(int argc, char* argv[]) {
        GalaxieSetup::setup();


        std::string appName = "ScorerTests";
        TempUtil::configureLogging (appName, argv);
        std::string propertyFileName = "ScorerTests.properties";
        auto beanFactory = std::make_unique<BeanFactory>("testVersion1.0");
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;
        beanFactory->initializeModules();


        std::shared_ptr<SvmSgdScorer> sc = std::make_shared<SvmSgdScorer>(
                beanFactory->deviceFeatureHistoryCassandraService.get());

        testScoring();
}
